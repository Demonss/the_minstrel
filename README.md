# The Minstrel 
```[v.0.0.0.1]```

## About:

The Minstrel is a Board Game about an adventurer and a storyteller.

## Version:

**format:** a.b.c.d

**where:**	

* a  --> the work version
* b  --> the large update
* c  --> the middle update
* d  --> the small update


## Git:

https://gitlab.com/Demonss/the_minstrel


## Directory:

/d/Users/Настолка/the_minstrel
